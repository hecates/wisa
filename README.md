>Your goal is to write a native Android application that implements an "Image Search" experience for English Wikipedia. The user will type a search term into an EditText field, and as they type they see pages whose titles start with the entered text, represented solely as images. Don't worry about pagination—just display the images found in the first 50 pages. Most importantly, try to deliver a smooth and pleasant user experience, and adhere to Material Design principles. Bonus points for making the app do something interesting when the user taps on one of the images (surprise us)!

# WISA #

Wisa stands for Wikipedia Image Search Application. How incredibly creative I am, right?

As I already contributed to the wikipedia app once (https://gerrit.wikimedia.org/r/#/c/347766/), it seemed to me that it would be more interesting for this exercise to actually do it "my way" instead of strictly following the wikimedia guidelines, in order to give you a better idea of my mindset and of the way I like to work. This doesn't mean in any way that I want to break the rules or anything, it simply sounded more logical to make this whole thing a bit more personal and, who knows, create some matter for an hypothetical discussion.

# Q&A #

### What the hell is this architecture? ###

It's a simplified reactive MVP architecture. It uses reactive patterns to observe & react to events. You guys told me on IRC that it would be cool to have a Rx-oriented PR in order to debate the pros/cons of the approach. Seemed to me like a good idea to do that as part of this exercise. 

It's cool right? No? You hate that idea? Damn.

I tried to add comments & refactor the code that makes it easier to understand if you're not familiar with the reactive patterns. 

Disclaimer : I f****** love Rx. 

### Why is there no UI test? ###

I only had a couple hours to spend on this test so I had to pick my battles. Writing simple espresso tests on an image gallery didn't appear to me like the highest priority, so I simply skipped that part. 

With the same logic I've avoided the fancy animations & many secondary features that would have created a better UX

### Why is there only one unit test? ###

Same thing. I could have written unit tests all night long, but once I had set up the test infrastructure & written a significant test illustrating the way my architecture can be easily tested, I thought the point was made. A prod-ready app would obviously require many more tests than that.

### Why no Dagger/Butterknife/other fancy libs? ###

I tried to keep the number of dependencies to the bare minimum according to my personal goals for this exercise : 

- rx for my architecture
- retrofit for the networking
- fresco for the pictures 

I also love Dagger and Butterknife, but it didn't seem to add much value in this simple exercise, so I stuck to the essential.

### No Fragments? ###

Fragments have a very tricky lifecycle that can make them quite painful to work with in many situations. I try to avoid them whenever possible. They can sometimes be useful, but in that case it would have been pointless to use them.

### Why are you using Bitbucket? ###

I don't want to publicly show this exercise for now, as my current employer is not aware of my application, that's why I haven't used my github account. I never used bitbucket before, so it was a good opportunity to try

I already regret that decision but it's already 3AM so... 

### Why are you repeating that you didn't have much time to work on this?###

I'm leaving for Iceland in 2 days, so I got about a million things to wrap up before. I could only spend a couple hours on this exercise yesterday & today, in the middle of the night, with a cup of coffee and my old friend : sleep deprivation. That's why I had to make choices about what I could do.

# Improvements #

Besides the ones I've already mentionne, here's a quick list of the things I would have done with more time : 

- connectivity : the app should react to connectivity changes. show an error message when it's lost, adapt the requested size of the thumbs on low bandwidth networks, etc. Very easy to do, but would require some RT permissions, so it seemed like overkill for this exercise
- thumbs sizes : seems like the thumbs are created on demand (thumbor?). It means that's it's strategically way smarter to request thumb sizes that are heavily used (and cached) in order to improve performance. I could not find any info regarding that, so I used an arbitrary value. I also abandoned the idea of progressively loading a better thumb quality in the full width image view.
- cache : Cache strategy can be improved
- configuration changes : this architecture would make it quite easy to store information in the presenter while views are destroyed & replay them when recreated. As the pictures are cached, there's not much difference in the app right now, but it would make it possible to retain, for example, the current scroll position.
- error handling : There's almost no error handling in this code. That's bad
- save researches : right now I only save (in shared prefs) the last query. It's actually a bad practice (for obvious confidentiality reasons) but I just wanted to show another aspect of the Rx infrastructure. We could make it much better by storing the opened pages and using them for autocomplete)