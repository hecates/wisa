package org.hecate.wisa.search;


import org.hecate.wisa.cache.CacheService;
import org.hecate.wisa.model.Image;
import org.hecate.wisa.model.PageImage;
import org.hecate.wisa.model.QueryResult;
import org.hecate.wisa.network.WikipediaService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.schedulers.TestScheduler;
import io.reactivex.subjects.PublishSubject;

import static org.hecate.wisa.search.SearchPresenter.DEBOUNCE_TIME_MS;
import static org.hecate.wisa.search.SearchPresenter.LAST_SEARCH;
import static org.hecate.wisa.search.SearchPresenter.SEARCH_LIMIT;
import static org.hecate.wisa.search.SearchPresenter.WANTED_THUMB_NAIL_SIZE_DP;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SearchPresenterTest {

    @Mock CacheService<String> cacheService;
    @Mock WikipediaService wikipediaService;

    private TestScheduler scheduler = new TestScheduler();

    @Mock Logger logger;
    @Mock SearchView view;

    private PublishSubject<String> inputTextSubject;
    private PublishSubject<PageImage> thumbSelectedSubject;
    private PublishSubject<Boolean> pageOpenedSubject;
    private PublishSubject<String> cachedValuesSubjet;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        SearchPresenter presenter = new SearchPresenter(wikipediaService, cacheService, scheduler, scheduler, logger);

        //setting up the publishSubjects that will be used as a sink to manually
        // trigger the observed events (input text changed, thumb selected, etc)

        inputTextSubject = PublishSubject.create();
        thumbSelectedSubject = PublishSubject.create();
        pageOpenedSubject = PublishSubject.create();
        cachedValuesSubjet = PublishSubject.create();

        //for each observed event, we set an "hidden subject", which means the
        // corresponding observers will only see its output
        when(view.observeInputTextChanged()).thenReturn(inputTextSubject.hide());
        when(view.observeThumbSelected()).thenReturn(thumbSelectedSubject.hide());
        when(view.observePageOpened()).thenReturn(pageOpenedSubject.hide());
        when(cacheService.load(LAST_SEARCH)).thenReturn(Observable.empty());

        // all observables will be ready to emit
        presenter.start(view);
    }

    @Test
    public void testSearchSuccessiveStrings() {
        // -- PREPARE -- //

        final String ignoredString = "ignored";
        final String lastStringEntered = "test input";
        // bad results have no usable thumbnail
        final QueryResult<PageImage> queryResult = buildQueryResult("result0", "bad-result1", "result2", "bad-result3");

        // no subject for that one. whenever it's called, we use an observable that will
        // immediately emit the result then complete
        when(wikipediaService.findImagesForPrefix(anyString(), anyInt(), anyInt(), anyInt()))
                .thenReturn(Observable.just(queryResult));

        //-- EXECUTE --

        // search input value has changed
        inputTextSubject.onNext(ignoredString);
        // only a millisecond has passed
        scheduler.advanceTimeBy(1, TimeUnit.MILLISECONDS);
        // search input value has changed again
        inputTextSubject.onNext(lastStringEntered);
        // nothing happened for DEBOUNCE_TIME_MS ms => we'll launch a query
        scheduler.advanceTimeBy(DEBOUNCE_TIME_MS, TimeUnit.MILLISECONDS);

        //-- ASSERT --

        verify(logger, times(1)).debug(anyString(), eq(lastStringEntered));
        verify(view, times(1)).showLoading();
        verify(wikipediaService, times(1)).findImagesForPrefix(lastStringEntered, WANTED_THUMB_NAIL_SIZE_DP, SEARCH_LIMIT, SEARCH_LIMIT);
        verify(view, times(1)).hideLoading();
        ArgumentCaptor<List> captor = ArgumentCaptor.forClass(List.class);
        verify(view, times(1)).updateGallery(captor.capture());
        // we make sure we ignored the bad results
        Assert.assertEquals(2, captor.getValue().size());
    }

    private QueryResult<PageImage> buildQueryResult(String... titles) {
        Map<Integer, PageImage> pages = new HashMap<>();

        for (int i = 0; i < titles.length; i++) {
            pages.put(i, new PageImage(i, titles[i], titles[i].startsWith("bad-") ? null : new Image("url" + titles[i])));
        }

        return new QueryResult<>(new QueryResult.Query<>(pages));
    }


}
