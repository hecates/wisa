package org.hecate.wisa;


import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.google.gson.Gson;

import org.hecate.wisa.cache.CacheService;
import org.hecate.wisa.cache.SharedPreferencesStringCacheService;
import org.hecate.wisa.network.WikipediaService;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class WisaApp extends Application {

    private static WikipediaService wikipediaService;
    private static CacheService<String> cacheService;

    @Override public void onCreate() {
        super.onCreate();
        Fresco.initialize(this);

        wikipediaService = new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .baseUrl(getString(R.string.wiki_base_url))
                .client(new OkHttpClient.Builder().addInterceptor(chain -> {
                    Request request = chain.request();
                    Response response = chain.proceed(request);
                    response.code();
                    //TODO HTTP handle errors here
                    return response;
                }).build())
                .build()
                .create(WikipediaService.class);

        cacheService = new SharedPreferencesStringCacheService(this, "wisa_string");
    }
    public static WikipediaService getWikipediaService() {
        return wikipediaService;
    }

    public static CacheService<String> getStringCacheService() {
        return cacheService;
    }
}
