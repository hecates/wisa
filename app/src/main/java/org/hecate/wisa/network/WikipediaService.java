package org.hecate.wisa.network;


import org.hecate.wisa.model.PageImage;
import org.hecate.wisa.model.PageURLInfo;
import org.hecate.wisa.model.QueryResult;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WikipediaService {

    @GET("api.php?action=query&prop=pageimages&format=json&piprop=thumbnail&generator=prefixsearch")
    Observable<QueryResult<PageImage>> findImagesForPrefix(@Query("gpssearch") String gpsSearch,
                                                           @Query("pithumbsize") Integer thumbSize,
                                                           @Query("pilimit") Integer piLimit,
                                                           @Query("gpslimit") Integer gpsLimit);

    @GET("api.php?action=query&prop=info&format=json&inprop=url")
    Observable<QueryResult<PageURLInfo>> findPageUrlInfo(@Query("pageids") Integer pageIds);

//    https://en.wikipedia.org/w/api.php?action=query&prop=info&pageids=12647606&inprop=url


}
