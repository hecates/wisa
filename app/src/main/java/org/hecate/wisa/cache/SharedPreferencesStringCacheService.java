package org.hecate.wisa.cache;


import android.content.Context;
import android.content.SharedPreferences;

import io.reactivex.Observable;

public class SharedPreferencesStringCacheService implements CacheService<String> {
    private final SharedPreferences preferences;

    public SharedPreferencesStringCacheService(Context context, String name) {
        this.preferences = context.getSharedPreferences(name, Context.MODE_PRIVATE);
    }
    @Override public void save(String key, String value) {
        preferences.edit().putString(key, value).apply();
    }

    @Override public Observable<String> load(String key) {
        return Observable.just(preferences.getString(key, ""));
    }
}
