package org.hecate.wisa.cache;


import io.reactivex.Observable;

public interface CacheService<T> {

    void save(String key, T value);

    Observable<T> load(String key);


}
