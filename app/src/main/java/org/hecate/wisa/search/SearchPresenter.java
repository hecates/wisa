package org.hecate.wisa.search;


import org.hecate.wisa.cache.CacheService;
import org.hecate.wisa.model.PageImage;
import org.hecate.wisa.network.WikipediaService;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

class SearchPresenter {

    static final int SEARCH_LIMIT = 50;
    static final int DEBOUNCE_TIME_MS = 500;
    static final String LAST_SEARCH = "stuff";
    public static final String DEFAULT_INITIAL_SEARCH = "Imagination";

    private CacheService<String> cacheService;
    private Scheduler ioScheduler;
    private Scheduler mainScheduler;
    private Logger logger;
    private WikipediaService wikipediaService;
    private PageImage currentPageImage;

    //the composite disposable will keep all our subscriptions and help us clear/dispose them all at once
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    static final Integer WANTED_THUMB_NAIL_SIZE_DP = 180;

    SearchPresenter(WikipediaService wikipediaService, CacheService<String> cacheService, Scheduler ioScheduler, Scheduler mainScheduler, Logger logger) {
        this.wikipediaService = wikipediaService;
        this.cacheService = cacheService;
        this.ioScheduler = ioScheduler;
        this.mainScheduler = mainScheduler;
        this.logger = logger;
    }

    void start(SearchView searchView) {
        searchView.initGallery(WANTED_THUMB_NAIL_SIZE_DP);

        compositeDisposable.addAll(
                handleSearchInputChanges(searchView),
                handleCurrentPageOpening(searchView),
                handleThumbSelection(searchView),
                handleInitialCachedValue(searchView)
        );
    }

    void pause() {
        compositeDisposable.clear();
    }

    void stop() {
        compositeDisposable.dispose();
    }

    private Disposable handleSearchInputChanges(SearchView searchView) {
        // Whenever text changes in the editttext, we :
        // - wait for DEBOUNCE_TIME_MS to throttle the input
        // - show the loading spinner
        // - call the API to get the corresponding images
        // - hide the spinner
        // - filter out the invalid images
        // - update the gallery

        return searchView.observeInputTextChanged()
                .doOnNext(s -> logger.debug("launch query with prefix : {}", s))
                .debounce(DEBOUNCE_TIME_MS, TimeUnit.MILLISECONDS, ioScheduler)
                .observeOn(mainScheduler)
                .doOnNext(value -> {
                    searchView.showLoading();
                    cacheService.save(LAST_SEARCH, value);
                })
                .flatMap(str -> wikipediaService.findImagesForPrefix(str, WANTED_THUMB_NAIL_SIZE_DP, SEARCH_LIMIT, SEARCH_LIMIT)
                        .subscribeOn(ioScheduler)
                        .observeOn(mainScheduler))
                .doOnNext(s -> searchView.hideLoading())
                .doOnTerminate(searchView::hideLoading)
                .subscribe(result -> {
                    List<PageImage> validPageImages = new ArrayList<>();
                    if (result.query() != null) {
                        Map<Integer, PageImage> pages = result.query().pages();
                        for (PageImage pageImage : pages.values()) {
                            if (pageImage.thumbnail() == null || pageImage.thumbnail().source() == null) {
                                logger.error("thumbnail or source is null - {}", pageImage.toString());
                                continue;
                            }
                            validPageImages.add(pageImage);
                        }
                    }
                    searchView.updateGallery(validPageImages);
                });
    }

    private Disposable handleThumbSelection(SearchView searchView) {
        // whenever a new thumb is selected, we only update the current page
        // everything else is view's responsibility. Not our business
        return searchView.observeThumbSelected().subscribe(pageImage -> currentPageImage = pageImage);
    }

    private Disposable handleCurrentPageOpening(SearchView searchView) {
        // whenever a page is opened (a tap on the selected -full-width- image) we :
        // - call the API to get the full URL of the corresponding wikipedia article
        // - ask the view to open the corresponding page (will open the app if installed, the browser if not)
        return searchView.observePageOpened().flatMap(_x ->
                wikipediaService.findPageUrlInfo(currentPageImage.pageId())
                        .map(pageURLInfoQueryResult -> pageURLInfoQueryResult.query().pages().get(currentPageImage.pageId()).fullURL())
                        .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()))
                .subscribe(searchView::openWikipediaPage);
    }

    private Disposable handleInitialCachedValue(SearchView searchView) {
        // use the last entered value to initialize the page (if any), or a given string by default
        return cacheService.load(LAST_SEARCH)
                .map(lastSearch -> !lastSearch.isEmpty() ? lastSearch : DEFAULT_INITIAL_SEARCH)
                .subscribe(searchView::setSearchInput);
    }
}
