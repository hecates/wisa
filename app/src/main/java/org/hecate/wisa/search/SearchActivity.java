package org.hecate.wisa.search;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.jakewharton.rxbinding2.view.RxView;
import com.jakewharton.rxbinding2.widget.RxTextView;

import org.hecate.wisa.R;
import org.hecate.wisa.WisaApp;
import org.hecate.wisa.model.PageImage;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

import static org.hecate.wisa.R.id.appbar;

public class SearchActivity extends AppCompatActivity implements SearchView {

    private EditText searchInputEditText;
    private RecyclerView searchResultsRecyclerView;
    private TextView emptyView;
    private ProgressBar searchProgressBar;
    private View resultZoom;
    private SimpleDraweeView resultZoomView;
    private AppBarLayout appBar;
    private TextView resultZoomText;
    private SearchPresenter presenter;
    private SearchResultAdapter adapter;

    private PublishSubject<PageImage> pageImagePublishSubject = PublishSubject.create();
    private boolean appbarExpanded;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        presenter = new SearchPresenter(
                WisaApp.getWikipediaService(),
                WisaApp.getStringCacheService(),
                Schedulers.io(),
                AndroidSchedulers.mainThread(),
                LoggerFactory.getLogger("search"));

        initViews();
    }

    @Override protected void onResume() {
        super.onResume();
        appbarExpanded = false;
        presenter.start(this);
    }

    @Override protected void onPause() {
        super.onPause();
        hideSoftKeyboard();
        presenter.pause();
    }

    @Override protected void onDestroy() {
        super.onDestroy();
        presenter.stop();
    }

    private void initViews() {
        searchInputEditText = (EditText) findViewById(R.id.search_input);
        searchResultsRecyclerView = (RecyclerView) findViewById(R.id.search_results);
        emptyView = (TextView) findViewById(R.id.empty_view);
        searchProgressBar = (ProgressBar) findViewById(R.id.search_progress_bar);
        resultZoom = findViewById(R.id.result_zoom);
        resultZoomView = (SimpleDraweeView) findViewById(R.id.result_zoom_view);
        appBar = (AppBarLayout) findViewById(appbar);
        resultZoomText = (TextView) findViewById(R.id.result_zoom_text);
        ImageView resulZoomClose = (ImageView) findViewById(R.id.result_zoom_close);

        appBar.findViewById(R.id.toolbar).setOnTouchListener((v, event) -> {
            expandAppBar(false);
            searchInputEditText.requestFocus();
            showKeyboard();
            return false;
        });

        resulZoomClose.setOnTouchListener((v, event) -> {
            expandAppBar(false);
            resultZoom.setVisibility(View.GONE);
            return true;
        });
    }
    private void expandAppBar(boolean expand) {
        if (!appbarExpanded && !expand) {
            return;
        }
        appBar.setExpanded(expand);
        appbarExpanded = expand;
    }

    @Override public void initGallery(int thumbWidth) {
        DisplayMetrics dm = getResources().getDisplayMetrics();
        float viewWidthDp = dm.widthPixels / dm.density;

        // nb of column is defined by the available screen width
        int nbCol = (int) (viewWidthDp / thumbWidth);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, nbCol);
        searchResultsRecyclerView.setLayoutManager(gridLayoutManager);
        adapter = new SearchResultAdapter();

        searchResultsRecyclerView.setAdapter(adapter);
        searchResultsRecyclerView.setOnTouchListener((view, _e) -> {
            hideSoftKeyboard();
            return false;
        });
    }

    @Override public Observable<String> observeInputTextChanged() {
        // will emit the content of EditText every time it changes
        return RxTextView.textChanges(searchInputEditText)
                .map(String::valueOf);
    }

    @Override public Observable<Boolean> observePageOpened() {
        // will emit true every time the full width image is tapped
        return RxView.clicks(resultZoom)
                .map(_x -> true);
    }

    @Override public Observable<PageImage> observeThumbSelected() {
        // will emit a pageimage object every time a thumb is tapped
        return pageImagePublishSubject.hide()
                .doOnNext(pageImage -> {
                    hideSoftKeyboard();
                    resultZoomView.setImageURI(pageImage.thumbnail().source());
                    resultZoom.setVisibility(View.VISIBLE);
                    resultZoomText.setText(pageImage.title());
                    expandAppBar(true);
                });
    }

    @Override public void showLoading() {
        searchProgressBar.setVisibility(View.VISIBLE);
    }

    @Override public void hideLoading() {
        searchProgressBar.setVisibility(View.GONE);
    }

    @Override public void updateGallery(List<PageImage> validPageImages) {
        adapter.pageImages = validPageImages;
        emptyView.setVisibility(validPageImages.isEmpty() ? View.VISIBLE : View.GONE);
        searchResultsRecyclerView.setVisibility(validPageImages.isEmpty() ? View.GONE : View.VISIBLE);
        adapter.notifyDataSetChanged();
    }

    @Override public void openWikipediaPage(String url) {
        Toast.makeText(getApplicationContext(), R.string.opening_wikipedia, Toast.LENGTH_SHORT).show();
        Intent articleIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(articleIntent);
    }

    @Override public void setSearchInput(String s) {
        searchInputEditText.setText(s);
        searchInputEditText.setSelection(s.length());
    }

    private void showKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    private void hideSoftKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(searchInputEditText.getWindowToken(), 0);
        searchProgressBar.clearFocus();
    }


    private class SearchResultAdapter extends RecyclerView.Adapter<SearchResultHolder> {
        List<PageImage> pageImages = new ArrayList<>();

        @Override public SearchResultHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_result, parent, false);
            return new SearchResultHolder(layoutView);
        }

        @Override public void onBindViewHolder(SearchResultHolder holder, int position) {
            PageImage pageImage = pageImages.get(position);
            holder.searchResultImageView.setContentDescription(pageImage.title());
            holder.searchResultImageView.setImageURI(pageImage.thumbnail().source());
            holder.pageImage = pageImage;
        }

        @Override public int getItemCount() {
            return pageImages.size();
        }
    }

    private class SearchResultHolder extends RecyclerView.ViewHolder {
        SimpleDraweeView searchResultImageView;
        PageImage pageImage;

        SearchResultHolder(View itemView) {
            super(itemView);
            searchResultImageView = (SimpleDraweeView) itemView.findViewById(R.id.search_result_image);
            RxView.clicks(searchResultImageView).subscribe(o -> pageImagePublishSubject.onNext(pageImage));
        }
    }
}