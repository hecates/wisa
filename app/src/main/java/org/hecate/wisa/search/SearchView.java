package org.hecate.wisa.search;


import org.hecate.wisa.model.PageImage;

import java.util.List;

import io.reactivex.Observable;

interface SearchView {

    void initGallery(int thumbWidth);
    Observable<String> observeInputTextChanged();
    Observable<Boolean> observePageOpened();
    Observable<PageImage> observeThumbSelected();
    void showLoading();
    void hideLoading();
    void updateGallery(List<PageImage> validPageImages);
    void openWikipediaPage(String url);
    void setSearchInput(String s);
}
