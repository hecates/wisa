package org.hecate.wisa.model;


import com.google.gson.annotations.SerializedName;

import java.util.Map;

public class QueryResult<T extends Page> {
    //ignoring unused fields for the sake of clarity

    @SerializedName("query") private Query<T> query;

    public static class Query<T> {
        @SerializedName("pages") private Map<Integer, T> pages;
        public Map<Integer, T> pages() {
            return pages;
        }
        public Query(Map<Integer, T> pages) {
            this.pages = pages;
        }
    }

    public Query<T> query() {
        return query;
    }

    public QueryResult(Query<T> query) {
        this.query = query;
    }
}



