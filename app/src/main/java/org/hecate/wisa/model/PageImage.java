package org.hecate.wisa.model;


import com.google.gson.annotations.SerializedName;

public class PageImage extends Page {
    @SerializedName("thumbnail") private Image thumbnail;

    public Image thumbnail() {
        return thumbnail;
    }

    public PageImage(Integer pageId, String title, Image thumbnail) {
        super(pageId, title);
        this.thumbnail = thumbnail;
    }
}
