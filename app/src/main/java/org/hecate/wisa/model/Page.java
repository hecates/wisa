package org.hecate.wisa.model;


import com.google.gson.annotations.SerializedName;

public class Page {
    @SerializedName("pageid") private Integer pageId;
    @SerializedName("title") private String title;

    public Integer pageId() {
        return pageId;
    }

    public String title() {
        return title;
    }

    Page(Integer pageId, String title) {
        this.pageId = pageId;
        this.title = title;
    }
}
