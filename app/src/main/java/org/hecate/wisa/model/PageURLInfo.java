package org.hecate.wisa.model;


import com.google.gson.annotations.SerializedName;

public class PageURLInfo extends Page {
    @SerializedName("fullurl") private String fullURL;

    public String fullURL() {
        return fullURL;
    }

    PageURLInfo(Integer pageId, String title, String fullURL) {
        super(pageId, title);
        this.fullURL = fullURL;
    }
}
