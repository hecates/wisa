package org.hecate.wisa.model;


import com.google.gson.annotations.SerializedName;

public class Image {
    @SerializedName("source") private String source;

    public String source() {
        return source;
    }

    public Image(String source) {
        this.source = source;
    }
}
